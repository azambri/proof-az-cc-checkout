# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

Checkout with Credit Card Project
This project is a checkout system developed in React with TypeScript. It allows users to select a product, enter their personal and credit card information, and complete the purchase. It's designed to offer an intuitive and secure shopping experience.

Key Features
Technologies Used: React, TypeScript, Redux (state management), styled-components (styling), React Router DOM (navigation).
Reducers: Global state is managed using three reducers: products, user, and card information.
Styling: styled-components is used to style the components, allowing for more modular and maintainable development.
Validations: Various validations are included in the input fields to ensure the accuracy and security of the entered data.
Unit Testing: Unit tests have been developed to ensure the correct functioning of the components, using Jest and React Testing Library.
Test Coverage: Test coverage of 93.1% has been achieved, ensuring greater reliability and stability of the code.
Running the Project
To run the project, follow these steps:

Clone the repository to your local machine.
Open a terminal in the project root directory.
Install dependencies using the command npm install.
Run the project with the command npm run start.
To run unit tests and obtain test coverage, use the command npm run test:coverage.
Directory Structure
The project follows an organized directory structure, which includes:

components: Contains all user interface components, separated by functionality.
store: Contains files related to the management of the application's global state.
slice: Contains the business-specific reducers.
assets: Stores static resources such as images, icons, etc.
Application Flow
The application flow follows these steps:

Product Selection: The user chooses a product from a list of available options.
Entering Personal Data: The user is prompted to enter their personal data, such as name, email, and phone number.
Entering Card Information: The user provides details of their credit card, including card number, expiration date, and CVC code.
Completing the Purchase: Once the required information is entered, the purchase is completed, and a confirmation message is displayed.
Enjoy your shopping experience with our secure and reliable checkout!

Coverage components
File | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
---------------------------|---------|----------|---------|---------|-------------------
All files | 93.14 | 89.28 | 73.46 | 93.1 |  
 components/Footer | 100 | 100 | 100 | 100 |  
 Footer.styles.ts | 100 | 100 | 100 | 100 |  
 Footer.tsx | 100 | 100 | 100 | 100 |  
 components/cardPay | 92.85 | 100 | 50 | 92.3 |  
 Card.style.ts | 100 | 100 | 100 | 100 |
CardPay.tsx | 87.5 | 100 | 50 | 85.71 | 16
components/customInputs | 96 | 89.28 | 91.17 | 96.29 |
CreditCardInput.tsx | 92.85 | 54.54 | 100 | 92.3 | 38,40
CustomInputMail.tsx | 100 | 100 | 100 | 100 |
CustomInputs.style.ts | 100 | 100 | 100 | 100 |
CvcInput.tsx | 91.66 | 90.9 | 80 | 90 | 14-15
DocumentTypeSelector.tsx | 100 | 100 | 100 | 100 |
InstallmentInput.tsx | 100 | 100 | 100 | 100 |
MonthSelector.tsx | 100 | 100 | 100 | 100 |
NameInput.tsx | 100 | 100 | 100 | 100 |
NumberInput.tsx | 100 | 100 | 100 | 100 |
PhoneInput.tsx | 100 | 100 | 100 | 100 |
TermsAndCodition.tsx | 94.44 | 100 | 75 | 100 |
YearSelector.tsx | 84.61 | 100 | 75 | 83.33 | 17-18
components/header | 94.11 | 87.5 | 66.66 | 93.33 |
Header.styles.ts | 100 | 100 | 100 | 100 |
Header.tsx | 92.3 | 50 | 66.66 | 90.9 | 14
components/loading | 100 | 100 | 100 | 100 |
Loading.style.ts | 100 | 100 | 100 | 100 |
Loading.tsx | 100 | 100 | 100 | 100 |
store | 100 | 100 | 100 | 100 |
store.ts | 100 | 100 | 100 | 100 |
store/slice | 65.21 | 0 | 0 | 65.21 |
CardSlice.ts | 71.42 | 100 | 0 | 71.42 | 40-43
ProductSlice.ts | 55.55 | 0 | 0 | 55.55 | 69-76
UserSlice.ts | 71.42 | 100 | 0 | 71.42 | 26-29
