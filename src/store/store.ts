import { configureStore } from "@reduxjs/toolkit";
import productosReducer from "./slice/ProductSlice";
import userReducer from "./slice/UserSlice";
import cardReducer from "./slice/CardSlice";

export const store = configureStore({
  reducer: {
    productos: productosReducer,
    user: userReducer,
    card: cardReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
