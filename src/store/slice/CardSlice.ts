import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface CardData {
  installments: number;
  numberCredit: string;
  idNumber: string;
  termsAccepted: boolean;
  isValidName: boolean;
  selectedType: string;
  selectedMonth: number;
  name: string;
  cvc: string;
  selectedYear: number;
}

interface CardState {
  cardData: CardData;
}

const initialState: CardState = {
  cardData: {
    installments: 1,
    numberCredit: "",
    idNumber: "",
    termsAccepted: false,
    isValidName: true,
    selectedType: "CC",
    selectedMonth: 1,
    name: "",
    cvc: "",
    selectedYear: new Date().getFullYear(),
  },
};

const userSlice = createSlice({
  name: "card",
  initialState,
  reducers: {
    updateCardData: (state, action: PayloadAction<CardData>) => {
      state.cardData = action.payload;
    },
    clearCardData: (state) => {
      state.cardData = initialState.cardData;
    },
  },
});

export const { updateCardData, clearCardData } = userSlice.actions;

export default userSlice.reducer;
