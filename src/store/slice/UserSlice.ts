import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface UserData {
  name: string;
  mail: string;
  phone: string;
}

interface UserState {
  userData: UserData;
}

const initialState: UserState = {
  userData: {
    name: "",
    mail: "",
    phone: "",
  },
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    updateUserData: (state, action: PayloadAction<UserData>) => {
      state.userData = action.payload;
    },
    clearUserData: (state) => {
      state.userData = initialState.userData;
    },
  },
});

export const { updateUserData, clearUserData } = userSlice.actions;

export default userSlice.reducer;
