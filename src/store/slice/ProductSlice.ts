import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface Producto {
  id: number;
  nombre: string;
  descripcion: string;
  valor: number;
  imagen: string;
}

interface ProductosState {
  lista: Producto[];
  productoSeleccionado: Producto | null;
  isModalOpen: boolean;
}

const initialState: ProductosState = {
  lista: [
    {
      id: 1,
      nombre: "Curso de Java SE",
      descripcion:
        "El curso de Java SE es una introducción completa al lenguaje de programación Java, cubriendo desde la sintaxis básica hasta conceptos avanzados de programación orientada a objetos. Ideal para principiantes, el curso proporciona una base sólida para desarrollar aplicaciones de software en Java",
      valor: 210,
      imagen: "java",
    },
    {
      id: 2,
      nombre: "Curso de Html 5",
      descripcion:
        "El curso de HTML5 es una guía completa para aprender las características más recientes de HTML. Los estudiantes explorarán nuevas etiquetas semánticas, elementos multimedia y capacidades avanzadas de formularios para mejorar la estructura y la interactividad de las páginas web. Este curso es esencial para aquellos que deseen dominar las últimas tecnologías web y crear sitios modernos y dinámicos.",
      valor: 30,
      imagen: "html",
    },
    {
      id: 3,
      nombre: "Curso de CSS con SC",
      descripcion:
        "El curso de Styled Components es una introducción práctica a esta popular biblioteca de React. A lo largo del curso, los estudiantes aprenderán a utilizar Styled Components para escribir y gestionar estilos de forma eficiente en sus aplicaciones React. Explorarán cómo crear estilos reutilizables y personalizables, así como a integrar Styled Components con otras bibliotecas y optimizar su rendimiento. Este curso es ideal para desarrolladores de React que deseen mejorar la modularidad y la legibilidad de sus estilos",
      valor: 60,
      imagen: "css",
    },
    {
      id: 4,
      nombre: "Curso de React",
      descripcion:
        "El curso de React es una guía completa para construir interfaces de usuario dinámicas con React. Cubre conceptos esenciales como la creación de componentes reutilizables y la gestión del estado, así como la integración de herramientas como React Router y Redux para aplicaciones más complejas. Ideal para desarrolladores web que buscan dominar React y crear interfaces de usuario eficientes y escalables.",
      valor: 200,
      imagen: "react",
    },
    {
      id: 5,
      nombre: "Curso de typescript",
      descripcion:
        "El curso de TypeScript es una guía completa para integrar tipado estático opcional y otras funcionalidades avanzadas en el desarrollo web. aprenderán a definir tipos para variables, funciones y objetos, permitirá detectar errores en tiempo de compilación y escribir un código más robusto y mantenible. Este curso cubre características avanzadas como tipos genéricos, interfaces y decoradores, siendo ideal para desarrolladores web que deseen mejorar su flujo de trabajo y crear aplicaciones más sólidas con TypeScript.",
      valor: 80,
      imagen: "typescript",
    },
  ],
  productoSeleccionado: null,
  isModalOpen: false,
};

const productosSlice = createSlice({
  name: "productos",
  initialState,
  reducers: {
    seleccionarProducto: (state, action: PayloadAction<number>) => {
      state.productoSeleccionado =
        state.lista.find((producto) => producto.id === action.payload) || null;
    },
    abrirModal: (state) => {
      state.isModalOpen = true;
    },
    cerrarModal: (state) => {
      state.isModalOpen = false;
    },
  },
});

export const { seleccionarProducto, abrirModal, cerrarModal } =
  productosSlice.actions;

export default productosSlice.reducer;
