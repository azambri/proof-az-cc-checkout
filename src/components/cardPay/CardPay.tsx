import React from "react";
import visa from "../../assets/visa.png";
import master from "../../assets/master.png";
import {
  CardContainer,
  ContentWrapper,
  ImagesWrapper,
  Text,
} from "./Card.style";

interface CardProps {
  handleGoToNextStep?: () => void;
}
const Card: React.FC<CardProps> = ({ handleGoToNextStep }: any) => {
  return (
    <CardContainer onClick={() => handleGoToNextStep()}>
      <ImagesWrapper>
        <img src={visa} alt="Imagen 1" width={35} />
        <img src={master} alt="Imagen 2" width={35} />
      </ImagesWrapper>
      <ContentWrapper>
        <Text>Paga con tus tarjetas</Text>
      </ContentWrapper>
    </CardContainer>
  );
};

export default Card;
