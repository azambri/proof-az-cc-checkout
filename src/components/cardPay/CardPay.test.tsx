import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Card from "./CardPay";
import visa from "../../assets/visa.png";
import master from "../../assets/master.png";

describe("Card component", () => {
  test("renders correctly", () => {
    const { getByText, getByAltText } = render(<Card />);

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("Paga con tus tarjetas")).toBeInTheDocument();

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByAltText("Imagen 1")).toHaveAttribute("src", visa);
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByAltText("Imagen 2")).toHaveAttribute("src", master);
  });
});
