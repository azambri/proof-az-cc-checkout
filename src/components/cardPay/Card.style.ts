import styled from "styled-components";
export const CardContainer = styled.div`
  width: 140px;
  height: 80px;
  border: 1px solid #ddd;
  border-radius: 8px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  @media (min-width: 768px) {
    width: 280px;
  }
  cursor: pointer;
`;
export const CardContainerNone = styled.div`
  width: 140px;
  height: 80px;
  border: 1px solid #ddd;
  background-color: #ddd;
  border-radius: 8px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  @media (min-width: 768px) {
    width: 280px;
  }
`;

export const ContentWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 8px;
`;

export const ImagesWrapper = styled.div`
  margin-right: 8px;
`;

export const Text = styled.p`
  text-align: center;
  font-size: 11px;
`;
