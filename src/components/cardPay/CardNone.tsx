import React from "react";
import { CardContainerNone } from "./Card.style";

const CardNone = () => {
  return <CardContainerNone />;
};

export default CardNone;
