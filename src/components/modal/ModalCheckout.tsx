import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../store/store";
import { cerrarModal } from "../../store/slice/ProductSlice";
import {
  ModalContent,
  ModalOverlay,
  CloseImg,
  GoBack,
} from "./ModalCheckout.style";
import StepOne from "./components/stepOne/StepOne";
import close from "../../assets/close.png";
import volver from "../../assets/volver.png";
import StepTwo from "./components/stepTwo/StepTwo";
import StepThree from "./components/stepThree/StepThree";

const ModalCheckout: React.FC = () => {
  const [step, setStep] = useState(0);
  const dispatch = useDispatch();
  const isModalOpen = useSelector(
    (state: RootState) => state.productos.isModalOpen
  );
  const handleCloseModal = () => {
    dispatch(cerrarModal());
  };
  const handleGoToNextStep = () => {
    setStep(step + 1);
  };
  const handleGoToPreviusStep = () => {
    if (step === 0) {
      handleCloseModal();
    } else {
      setStep(step - 1);
    }
  };

  if (!isModalOpen) return null;
  return (
    <ModalOverlay onClick={handleCloseModal}>
      <ModalContent onClick={(e) => e.stopPropagation()}>
        <CloseImg onClick={handleCloseModal}>
          <img src={close} alt="close" width={15} />
        </CloseImg>
        <GoBack>
          <div onClick={handleGoToPreviusStep}>
            <img src={volver} alt="volver" width={40} />
          </div>
          <div className="text-goBack">Volver</div>
        </GoBack>
        {step === 0 && <StepOne handleGoToNextStep={handleGoToNextStep} />}
        {step === 1 && <StepTwo handleGoToNextStep={handleGoToNextStep} />}
        {step === 2 && <StepThree />}
      </ModalContent>
    </ModalOverlay>
  );
};

export default ModalCheckout;
