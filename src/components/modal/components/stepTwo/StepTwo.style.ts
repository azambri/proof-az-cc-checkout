import styled from "styled-components";
export const StepTwoContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-bottom: 32px;
`;

export const ButtonContinue = styled.button`
  border: none;
  background-color: #6b5b95;
  color: #ffffff;
  font-size: 18px;
  padding: 10px 20px;
  border-radius: 5px;
  margin-top: 16px;
  cursor: pointer;
  &:disabled {
    background-color: #555;
    cursor: not-allowed;
  }
`;
export const Title = styled.div`
  font-family: roboto;
  font-size: 28px;
  font-weight: bold;
  color: #333;
  margin-bottom: 16px;
  @media (max-width: 767px) {
    width: 300px;
  }
`;
export const CvcQuotas = styled.div`
  display: flex;
  .quotas{
    margin-left: 16px;
  }
`;
