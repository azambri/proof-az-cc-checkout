import React, { useState } from "react";
import { ButtonContinue, StepTwoContainer, Title } from "./StepTwo.style";
import { useSelector, useDispatch } from "react-redux";
import CustomInputMail from "../../../customInputs/CustomInputMail";
import NameInput from "../../../customInputs/NameInput";
import PhoneInput from "../../../customInputs/PhoneInput";
import { RootState } from "../../../../store/store";
import { updateUserData } from "../../../../store/slice/UserSlice";

interface CardProps {
  handleGoToNextStep?: () => void;
}
const StepTwo: React.FC<CardProps> = ({ handleGoToNextStep }: any) => {
  const dispatch = useDispatch();
  const userData = useSelector((state: RootState) => state.user.userData);
  const [mail, setMail] = useState(userData.mail);
  const [name, setName] = useState(userData.name);
  const [phone, setPhone] = useState(userData.phone);
  const [isValidMail, setIsValidMail] = useState(true);
  const [isValidName, setIsValidName] = useState(true);
  const [isValidPhone, setIsValidPhone] = useState(true);
  const handleValidateData = () => {
    return !(isValidMail && isValidName && isValidPhone);
  };
  const handleUpdateUserData = () => {
    dispatch(updateUserData({ name, mail, phone }));
  };
  const handleGoToCard = () => {
    handleUpdateUserData();
    handleGoToNextStep();
  };
  return (
    <StepTwoContainer>
      <Title>Ingresa tus datos</Title>

      <CustomInputMail
        label="Correo electrónico"
        placeholder="Introduce tu correo electrónico"
        mail={mail}
        setMail={setMail}
        isValidMail={isValidMail}
        setIsValidMail={setIsValidMail}
      />
      <NameInput
        label="Nombres y Apellidos"
        placeholder="Introduce tu nombre completo"
        name={name}
        setName={setName}
        isValidName={isValidName}
        setIsValidName={setIsValidName}
      />
      <PhoneInput
        label="Celular"
        placeholder="Introduce tu numero telefonico"
        phone={phone}
        setPhone={setPhone}
        isValidPhone={isValidPhone}
        setIsValidPhone={setIsValidPhone}
      />
      <ButtonContinue onClick={handleGoToCard} disabled={handleValidateData()}>
        Continuar con tu pago
      </ButtonContinue>
    </StepTwoContainer>
  );
};

export default StepTwo;
