import styled from "styled-components";
export const StepOneContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 32px;
`;
