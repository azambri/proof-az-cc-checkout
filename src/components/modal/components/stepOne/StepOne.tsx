import React from "react";
import Card from "../../../cardPay/CardPay";
import CardNone from "../../../cardPay/CardNone";
import { StepOneContainer } from "./StepOne.style";

const StepOne = ({ handleGoToNextStep }: any) => {
  return (
    <>
      <StepOneContainer>
        <Card handleGoToNextStep={handleGoToNextStep} />
        <CardNone />
      </StepOneContainer>
      <StepOneContainer>
        <CardNone />
        <CardNone />
      </StepOneContainer>
      <StepOneContainer>
        <CardNone />
        <CardNone />
      </StepOneContainer>
    </>
  );
};

export default StepOne;
