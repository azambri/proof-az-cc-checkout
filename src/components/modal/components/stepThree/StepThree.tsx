import React, { useState, useEffect } from "react";
import {
  ButtonContinue,
  StepTwoContainer,
  CvcQuotas,
} from "../stepTwo/StepTwo.style";
import { useNavigate } from "react-router-dom";
import CreditCardInput from "../../../customInputs/CreditCardInput";
import NameInput from "../../../customInputs/NameInput";
import MonthSelector from "../../../customInputs/MonthSelector";
import { InputLabel } from "../../../customInputs/CustomInputs.style";
import YearSelector from "../../../customInputs/YearSelector";
import DocumentTypeSelector from "../../../customInputs/DocumentTypeSelector";
import NumberInput from "../../../customInputs/NumberInput";
import CvcInput from "../../../customInputs/CvcInput";
import InstallmentInput from "../../../customInputs/InstallmentInput";
import TermsAndCodition from "../../../customInputs/TermsAndCodition";
import { RootState } from "../../../../store/store";
import { useDispatch, useSelector } from "react-redux";
import { updateCardData } from "../../../../store/slice/CardSlice";

const StepThree = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const cardData = useSelector((state: RootState) => state.card.cardData);
  const [installments, setInstallments] = useState<number>(
    cardData.installments
  );
  const [numberCredit, setNumberCredit] = useState(cardData.numberCredit);
  const [idNumber, setValueID] = useState<string>(cardData.idNumber);
  const [termsAccepted, setTermsAccepted] = useState(cardData.termsAccepted);
  const [isValidName, setIsValidName] = useState(cardData.isValidName);
  const [selectedType, setSelectedType] = useState<string>(
    cardData.selectedType
  );
  const [selectedMonth, setSelectedMonth] = useState<number>(
    cardData.selectedMonth
  );
  const [name, setName] = useState(cardData.name);
  const [cvc, setCvc] = useState<string>(cardData.cvc);
  const [selectedYear, setSelectedYear] = useState<number>(
    cardData.selectedYear
  );
  const [isFormValid, setIsFormValid] = useState(false);

  const handleSelectType = (type: string) => {
    setSelectedType(type);
  };
  const handleSelectMonth = (month: number) => {
    setSelectedMonth(month);
  };

  const handleSelectYear = (year: number) => {
    setSelectedYear(year);
  };
  const handleChangeID = (newValue: string) => {
    setValueID(newValue);
  };

  const handleChangeCvc = (newValue: string) => {
    setCvc(newValue);
  };

  const handleChangeQuotas = (newValue: number) => {
    setInstallments(newValue);
  };

  useEffect(() => {
    if (numberCredit && name && cvc && idNumber && termsAccepted) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [numberCredit, name, cvc, idNumber, termsAccepted]);

  const handleGoToNextStep = () => {
    handleUpdateUserData();
    navigate("/end-page");
  };
  const handleUpdateUserData = () => {
    dispatch(
      updateCardData({
        installments,
        numberCredit,
        idNumber,
        termsAccepted,
        isValidName,
        selectedType,
        selectedMonth,
        name,
        cvc,
        selectedYear,
      })
    );
  };
  return (
    <StepTwoContainer>
      <CreditCardInput
        value={numberCredit}
        onChange={setNumberCredit}
        label="Numero en la  tarjeta"
      />
      <NameInput
        label="Nombre en la  tarjeta"
        name={name}
        setName={setName}
        isValidName={isValidName}
        setIsValidName={setIsValidName}
      />
      <InputLabel>Expira el </InputLabel>
      <div>
        <MonthSelector
          selectedMonth={selectedMonth}
          onSelectMonth={handleSelectMonth}
        />
        <YearSelector
          selectedYear={selectedYear}
          onSelectYear={handleSelectYear}
        />
      </div>
      <CvcQuotas>
        <div>
          <InputLabel>CVC</InputLabel>
          <CvcInput value={cvc} onChange={handleChangeCvc} />
        </div>
        <div className="quotas">
          <InputLabel>Cuotas</InputLabel>
          <InstallmentInput
            value={installments}
            onChange={handleChangeQuotas}
          />
        </div>
      </CvcQuotas>
      <div>
        <InputLabel>Identificación del tarjetahabiente </InputLabel>
        <DocumentTypeSelector
          selectedType={selectedType}
          onSelectType={handleSelectType}
        />
        <NumberInput value={idNumber} onChange={handleChangeID} />
      </div>

      <div>
        <TermsAndCodition
          termsAccepted={termsAccepted}
          setTermsAccepted={setTermsAccepted}
        />
      </div>
      <ButtonContinue onClick={handleGoToNextStep} disabled={!isFormValid}>
        Continuar con tu pago
      </ButtonContinue>
    </StepTwoContainer>
  );
};

export default StepThree;
