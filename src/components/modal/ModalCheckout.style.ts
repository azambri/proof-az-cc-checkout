import styled from "styled-components";

export const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  overflow-y: auto;
`;

export const ModalContent = styled.div`
  background-color: white;
  width: 80%;
  max-width: 600px;
  height: 65vh;
  border-radius: 8px;
  padding: 20px;
  overflow-y: auto;
`;
export const CloseImg = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;
export const GoBack = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;
  .text-goBack {
    padding-bottom: 6px;
  }
`;
