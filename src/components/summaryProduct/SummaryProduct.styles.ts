import styled from "styled-components";

export const SummaryProductContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 16px 0;
  .title-course {
    display: flex;
    max-width: 260px;
    justify-content: space-between;
    width: 400px;
    text-align: justify;
  }
  .description {
    display: flex;
    justify-content: space-between;
  }

  @media (max-width: 767px) {
    .description {
      width: 300px;
    }
  }

  @media (min-width: 768px) and (max-width: 1279px) {
    .description {
      width: 640px;
    }
  }

  @media (min-width: 1280px) {
    .description {
      width: 800px;
    }
  }
`;
export const TitleDescription = styled.h2`
  font-family: roboto;
  font-size: 24px;
  font-weight: bold;
  color: #333;
  margin-bottom: 20px;
`;
