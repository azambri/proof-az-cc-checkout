import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import imagejava from "../../assets/java.png";
import imagehtml from "../../assets/html.png";
import imagecss from "../../assets/css.png";
import imagereact from "../../assets/react.png";
import imagetypescript from "../../assets/ts.png";

import {
  SummaryProductContainer,
  TitleDescription,
} from "./SummaryProduct.styles";

const SumaryProduct = () => {
  const productoSeleccionado = useSelector(
    (state: RootState) => state.productos.productoSeleccionado
  );
  type ImageName = "java" | "html" | "css" | "react" | "typescript";

  const images: Record<ImageName, string> = {
    java: imagejava,
    html: imagehtml,
    css: imagecss,
    react: imagereact,
    typescript: imagetypescript,
  };

  const handleReturnImagen = (image: ImageName): string => {
    return images[image] || imagejava;
  };

  return (
    <>
      {productoSeleccionado ? (
        <SummaryProductContainer>
          <TitleDescription>Producto Seleccionado</TitleDescription>
          <div className="title-course">
            <img
              src={handleReturnImagen(productoSeleccionado.imagen as ImageName)}
              alt={productoSeleccionado.nombre}
              height={40}
            />
            <p>{productoSeleccionado.nombre}</p>
            <p>${productoSeleccionado.valor} USD</p>
          </div>
          <p className="description">{productoSeleccionado.descripcion}</p>
        </SummaryProductContainer>
      ) : (
        <div></div>
      )}
    </>
  );
};

export default SumaryProduct;
