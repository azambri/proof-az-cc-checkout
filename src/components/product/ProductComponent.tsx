import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../store/store";
import { seleccionarProducto } from "../../store/slice/ProductSlice";
import SummaryProduct from "../summaryProduct/SummaryProduct";
import { ListProductContainer, TitleCourses } from "./ProductComponent.style";

const ProductComponent: React.FC = () => {
  const productos = useSelector((state: RootState) => state.productos.lista);

  const dispatch = useDispatch();

  const handleSeleccionarProducto = (id: number) => {
    dispatch(seleccionarProducto(id));
  };

  return (
    <ListProductContainer>
      <TitleCourses>Seleccione el curso que desea comprar </TitleCourses>
      <ul>
        {productos.map((producto) => (
          <li
            className="pointer-li"
            key={producto.id}
            onClick={() => handleSeleccionarProducto(producto.id)}
          >
            {producto.nombre}
          </li>
        ))}
      </ul>
      <SummaryProduct />
    </ListProductContainer>
  );
};

export default ProductComponent;
