import styled from "styled-components";

export const TitleCourses = styled.h1`
  font-family: roboto;
  font-size: 28px;
  font-weight: bold;
  color: #333;
  margin-bottom: 20px;
  @media (max-width: 767px) {
    width: 300px;
  }
`;
export const ListProductContainer = styled.div`
  .pointer-li {
    margin-bottom: 8px;
    cursor: pointer;
  }
`;
