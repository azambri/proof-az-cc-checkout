import { render } from "@testing-library/react";
import Loading from "./Loading";

describe("Loading component", () => {
  test("renders loading spinner", () => {
    const { getByTestId } = render(<Loading />);

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const loadingContainer = getByTestId("loading");
    expect(loadingContainer).toBeInTheDocument();

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const spinner = getByTestId("spiner");
    expect(spinner).toBeInTheDocument();
  });
});
