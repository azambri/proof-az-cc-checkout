import React from "react";
import { LoadingContainer, Spinner } from "./Loading.style";

const Loading: React.FC = () => {
  return (
    <LoadingContainer data-testid="loading">
      <Spinner data-testid="spiner" />
    </LoadingContainer>
  );
};

export default Loading;
