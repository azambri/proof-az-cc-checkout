import React from "react";
import { render } from "@testing-library/react";
import Footer from "./Footer";

describe("Footer component", () => {
  test("renders correctly", () => {
    const { getByText }: any = render(<Footer />);

    expect(
      // eslint-disable-next-line testing-library/prefer-screen-queries
      getByText("by Alex Eduardo Zambrano Perez 2024")
    ).toBeInTheDocument();

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const linkElement = getByText(
      "by Alex Eduardo Zambrano Perez 2024"
      // eslint-disable-next-line testing-library/no-node-access
    ).closest("a");
    expect(linkElement).toHaveAttribute(
      "href",
      "https://www.linkedin.com/in/alex-eduardo-zambrano-perez-aa445915b/"
    );
  });
});
