import React from "react";
import { render, fireEvent } from "@testing-library/react";
import DocumentTypeSelector from "./DocumentTypeSelector";

describe("DocumentTypeSelector component", () => {
  test("renders correctly", () => {
    // Renderizar el componente
    const { getByText } = render(
      <DocumentTypeSelector selectedType="TI" onSelectType={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("TI")).toBeInTheDocument();
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("CC")).toBeInTheDocument();
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("CE")).toBeInTheDocument();
  });

  test("handles type selection correctly", () => {
    const handleSelectType = jest.fn();

    const { getByRole } = render(
      <DocumentTypeSelector selectedType="TI" onSelectType={handleSelectType} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.change(getByRole("combobox"), { target: { value: "CC" } });

    expect(handleSelectType).toHaveBeenCalledWith("CC");
  });
});
