import React from "react";
import { render, fireEvent } from "@testing-library/react";
import PhoneInput from "./PhoneInput";

describe("PhoneInput component", () => {
  test("renders correctly", () => {
    const { getByPlaceholderText } = render(
      <PhoneInput
        label="Teléfono"
        placeholder="Ingrese su teléfono"
        phone=""
        setPhone={() => {}}
        isValidPhone={true}
        setIsValidPhone={() => {}}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByPlaceholderText("Ingrese su teléfono")).toBeInTheDocument();
  });

  test("handles phone change correctly", () => {
    const setPhone = jest.fn();

    const { getByPlaceholderText } = render(
      <PhoneInput
        label="Teléfono"
        placeholder="Ingrese su teléfono"
        phone=""
        setPhone={setPhone}
        isValidPhone={true}
        setIsValidPhone={() => {}}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const input = getByPlaceholderText("Ingrese su teléfono");
    fireEvent.change(input, { target: { value: "1234567890" } });

    expect(setPhone).toHaveBeenCalledWith("1234567890");
  });

  test("displays error message for invalid phone", () => {
    const { getByText } = render(
      <PhoneInput
        label="Teléfono"
        placeholder="Ingrese su teléfono"
        phone="12345"
        setPhone={() => {}}
        isValidPhone={false}
        setIsValidPhone={() => {}}
      />
    );

    expect(
      // eslint-disable-next-line testing-library/prefer-screen-queries
      getByText("Ingrese un número de teléfono válido (10 dígitos)")
    ).toBeInTheDocument();
  });
});
