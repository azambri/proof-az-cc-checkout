import { render, fireEvent } from "@testing-library/react";
import NameInput from "./NameInput";

describe("NameInput component", () => {
  test("renders correctly", () => {
    const { getByPlaceholderText } = render(
      <NameInput
        label="Nombre"
        placeholder="Ingrese su nombre"
        name=""
        setName={() => {}}
        isValidName={true}
        setIsValidName={() => {}}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByPlaceholderText("Ingrese su nombre")).toBeInTheDocument();
  });

  test("handles name change correctly", () => {
    const setName = jest.fn();

    const { getByPlaceholderText } = render(
      <NameInput
        label="Nombre"
        placeholder="Ingrese su nombre"
        name=""
        setName={setName}
        isValidName={true}
        setIsValidName={() => {}}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const input = getByPlaceholderText("Ingrese su nombre");
    fireEvent.change(input, { target: { value: "John Doe" } });

    expect(setName).toHaveBeenCalledWith("John Doe");
  });

  test("displays error message for invalid name", () => {
    const { getByText } = render(
      <NameInput
        label="Nombre"
        placeholder="Ingrese su nombre"
        name="123"
        setName={() => {}}
        isValidName={false}
        setIsValidName={() => {}}
      />
    );

    expect(
      // eslint-disable-next-line testing-library/prefer-screen-queries
      getByText("Ingrese un nombre válido (solo texto)")
    ).toBeInTheDocument();
  });
});
