import React, { useState } from "react";
import styled from "styled-components";
interface TermsAndCoditionIputsProps {
  termsAccepted: boolean;
  setTermsAccepted: React.Dispatch<React.SetStateAction<boolean>>;
}

const TermsAndCodition: React.FC<TermsAndCoditionIputsProps> = ({
  termsAccepted,
  setTermsAccepted,
}: any) => {
  const [showModal, setShowModal] = useState(false);

  const handleCheckboxChange = () => {
    setTermsAccepted(!termsAccepted);
  };

  const handleTermsClick = () => {
    setShowModal(true);
    setTimeout(() => setShowModal(false), 3000);
  };

  return (
    <div>
      <label>
        <input
          type="checkbox"
          checked={termsAccepted}
          onChange={handleCheckboxChange}
        />
        Acepto los términos y condiciones
      </label>
      <TermsLink onClick={handleTermsClick}>
        {" "}
        Ver términos y condiciones
      </TermsLink>
      {showModal && (
        <Modal>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </Modal>
      )}
    </div>
  );
};

const Modal = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
`;

const TermsLink = styled.a`
  margin-left: 10px;
  color: #6b5b95;
  cursor: pointer;
`;

export default TermsAndCodition;
