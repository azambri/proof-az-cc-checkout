import React from "react";
import { Select } from "./CustomInputs.style";

interface MonthSelectorProps {
  selectedMonth: number;
  onSelectMonth: (month: number) => void;
}

const months = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];

const MonthSelector: React.FC<MonthSelectorProps> = ({
  selectedMonth,
  onSelectMonth,
}) => {
  return (
    <Select
      value={selectedMonth}
      onChange={(e) => onSelectMonth(parseInt(e.target.value))}
    >
      {months.map((month, index) => (
        <option key={index} value={index + 1}>{`${
          index + 1
        } - ${month}`}</option>
      ))}
    </Select>
  );
};

export default MonthSelector;
