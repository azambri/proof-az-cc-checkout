import React, { useEffect, useState } from "react";
import styled from "styled-components";
import cvc from "../../assets/cvc.png";

interface CvcInputProps {
  value: string;
  onChange: (newValue: string) => void;
}

const CvcInput: React.FC<CvcInputProps> = ({ value, onChange }) => {
  const [showImage, setShowImage] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      if (showImage === true) {
        setShowImage(false);
      }
    }, 2000);
  }, [showImage]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value.replace(/\D/g, "");
    onChange(newValue);
  };

  const handleIconClick = () => {
    setShowImage(!showImage);
  };

  return (
    <InputContainer>
      <Input
        type="text"
        value={value}
        onChange={handleChange}
        maxLength={3}
        placeholder="CVC"
        title="El CVC (Card Verification Code) es un número de seguridad de 3 dígitos que se encuentra en la parte posterior de tu tarjeta."
      />
      <Tooltip onClick={handleIconClick}>ℹ️</Tooltip>
      {showImage && <Image src={cvc} alt="CVC Location" />}
    </InputContainer>
  );
};

const InputContainer = styled.div`
  position: relative;
`;

const Input = styled.input`
  padding: 8px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
`;

const Tooltip = styled.span`
  position: absolute;
  top: 50%;
  right: 8px;
  transform: translateY(-50%);
  cursor: pointer;
`;

const Image = styled.img`
  position: absolute;
  top: calc(100% + 5px);
  left: 50%;
  transform: translateX(-50%);
  width: 150px;
`;

export default CvcInput;
