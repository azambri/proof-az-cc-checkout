import React from "react";
import { render, fireEvent } from "@testing-library/react";
import CreditCardInput from "./CreditCardInput";

describe("CreditCardInput component", () => {
  test("renders correctly", () => {
    const { getByPlaceholderText } = render(
      <CreditCardInput label="Número de tarjeta" value="" onChange={() => {}} />
    );

    expect(
      // eslint-disable-next-line testing-library/prefer-screen-queries
      getByPlaceholderText("Número de tarjeta de crédito")
    ).toBeInTheDocument();
  });

  test("handles input change correctly", () => {
    const handleChange = jest.fn();

    const { getByPlaceholderText } = render(
      <CreditCardInput
        label="Número de tarjeta"
        value=""
        onChange={handleChange}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const inputElement = getByPlaceholderText("Número de tarjeta de crédito");
    fireEvent.change(inputElement, { target: { value: "1234" } });

    // Verificar que la función de manejo de cambio se haya llamado con el valor correcto
    expect(handleChange).toHaveBeenCalledWith("1234");
  });

  test("handles key down event correctly", () => {
    const handleKeyDown = jest.fn();

    const { getByPlaceholderText } = render(
      <CreditCardInput label="Número de tarjeta" value="" onChange={() => {}} />
    );
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const inputElement = getByPlaceholderText("Número de tarjeta de crédito");
    fireEvent.keyDown(inputElement, { key: "a" });

    expect(handleKeyDown).not.toHaveBeenCalledWith("a");
  });
});
