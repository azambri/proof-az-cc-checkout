import { render, fireEvent } from "@testing-library/react";
import YearSelector from "./YearSelector";

describe("YearSelector component", () => {
  test("renders correctly", () => {
    const { getByText } = render(
      <YearSelector selectedYear={2024} onSelectYear={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("2034")).toBeInTheDocument();
  });
});
