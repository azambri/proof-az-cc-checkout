import { render, fireEvent } from "@testing-library/react";
import InstallmentInput from "./InstallmentInput";

describe("InstallmentInput component", () => {
  test("renders correctly", () => {
    const { getByText } = render(
      <InstallmentInput value={1} onChange={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("1")).toBeInTheDocument();
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("48")).toBeInTheDocument();
  });

  test("handles value change correctly", () => {
    const handleChange = jest.fn();

    const { getByRole } = render(
      <InstallmentInput value={1} onChange={handleChange} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.change(getByRole("combobox"), { target: { value: "10" } });

    expect(handleChange).toHaveBeenCalledWith(10);
  });
});
