import React from "react";
import {
  ErrorMessage,
  InputContainer,
  InputField,
  InputLabel,
} from "./CustomInputs.style";

interface NameInputProps {
  label: string;
  placeholder?: string;
  name: string;
  setName: React.Dispatch<React.SetStateAction<string>>;
  isValidName: boolean;
  setIsValidName: React.Dispatch<React.SetStateAction<boolean>>;
}

const NameInput: React.FC<NameInputProps> = ({
  label,
  placeholder,
  name,
  setName,
  isValidName,
  setIsValidName,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
    const isValidName = /^[a-zA-Z]+(?:\s+[a-zA-Z]+)*$/.test(name);
    setIsValidName(isValidName);
  };

  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <InputField
        type="text"
        placeholder={placeholder}
        value={name}
        onChange={handleChange}
        style={{ borderColor: isValidName ? "initial" : "red" }}
      />
      {!isValidName && (
        <ErrorMessage>Ingrese un nombre válido (solo texto)</ErrorMessage>
      )}
    </InputContainer>
  );
};

export default NameInput;
