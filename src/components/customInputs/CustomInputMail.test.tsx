import React from "react";
import { render, fireEvent } from "@testing-library/react";
import CustomInputMail from "./CustomInputMail";

describe("CustomInputMail component", () => {
  test("renders correctly", () => {
    const { getByText, getByPlaceholderText } = render(
      <CustomInputMail
        label="Correo electrónico"
        placeholder="Ingrese su correo electrónico"
        mail=""
        setMail={() => {}}
        isValidMail={true}
        setIsValidMail={() => {}}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("Correo electrónico")).toBeInTheDocument();
    expect(
      // eslint-disable-next-line testing-library/prefer-screen-queries
      getByPlaceholderText("Ingrese su correo electrónico")
    ).toBeInTheDocument();
  });

  test("handles input change correctly", () => {
    const setMail = jest.fn();
    const setIsValidMail = jest.fn();

    const { getByPlaceholderText } = render(
      <CustomInputMail
        label="Correo electrónico"
        placeholder="Ingrese su correo electrónico"
        mail=""
        setMail={setMail}
        isValidMail={true}
        setIsValidMail={setIsValidMail}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const inputElement = getByPlaceholderText("Ingrese su correo electrónico");
    fireEvent.change(inputElement, { target: { value: "test@example.com" } });

    expect(setIsValidMail).toHaveBeenCalledWith(false);
  });

  test("displays error message for invalid email", () => {
    const { getByText, getByPlaceholderText } = render(
      <CustomInputMail
        label="Correo electrónico"
        placeholder="Ingrese su correo electrónico"
        mail="invalidEmail"
        setMail={() => {}}
        isValidMail={false}
        setIsValidMail={() => {}}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("Correo electrónico inválido")).toBeInTheDocument();

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const inputElement = getByPlaceholderText("Ingrese su correo electrónico");
    expect(inputElement).toHaveStyle("borderColor: red");
  });
});
