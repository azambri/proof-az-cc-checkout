import React from "react";
import { render, fireEvent } from "@testing-library/react";
import NumberInput from "./NumberInput";

describe("NumberInput component", () => {
  test("renders correctly", () => {
    const { getByRole } = render(
      <NumberInput value="123" onChange={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const input = getByRole("textbox");
    expect(input).toBeInTheDocument();
    expect(input).toHaveValue("123");
  });

  test("handles value change correctly", () => {
    const handleChange = jest.fn();

    const { getByRole } = render(
      <NumberInput value="123" onChange={handleChange} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const input = getByRole("textbox");
    fireEvent.change(input, { target: { value: "456" } });

    expect(handleChange).toHaveBeenCalledWith("456");
  });

  test("only allows numeric input", () => {
    const handleChange = jest.fn();

    const { getByRole } = render(
      <NumberInput value="123" onChange={handleChange} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const input = getByRole("textbox");
    fireEvent.change(input, { target: { value: "abc" } });

    expect(handleChange).toHaveBeenCalledWith("");
  });
});
