import React from "react";
import styled from "styled-components";

interface DocumentTypeSelectorProps {
  selectedType: string;
  onSelectType: (type: string) => void;
}

const DocumentTypeSelector: React.FC<DocumentTypeSelectorProps> = ({
  selectedType,
  onSelectType,
}) => {
  const types = ["TI", "CC", "CE"];

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedType = event.target.value;
    onSelectType(selectedType);
  };

  return (
    <Select value={selectedType} onChange={handleChange}>
      {types.map((type) => (
        <option key={type} value={type}>
          {type}
        </option>
      ))}
    </Select>
  );
};

const Select = styled.select`
  padding: 8px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
`;

export default DocumentTypeSelector;
