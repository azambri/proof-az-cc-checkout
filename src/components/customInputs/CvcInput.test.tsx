import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import CvcInput from "./CvcInput";

describe("CvcInput component", () => {
  test("renders correctly", () => {
    const { getByPlaceholderText } = render(
      <CvcInput value="" onChange={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByPlaceholderText("CVC")).toBeInTheDocument();
  });

  test("handles input change correctly", () => {
    const handleChange = jest.fn();

    const { getByPlaceholderText } = render(
      <CvcInput value="" onChange={handleChange} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const inputElement = getByPlaceholderText("CVC");
    fireEvent.change(inputElement, { target: { value: "123" } });

    expect(handleChange).toHaveBeenCalledWith("123");
  });

  test("displays tooltip and image when icon is clicked", async () => {
    const { getByText, getByAltText } = render(
      <CvcInput value="" onChange={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.click(getByText("ℹ️"));

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("ℹ️")).toBeInTheDocument();

    await waitFor(() => {
      // eslint-disable-next-line testing-library/prefer-screen-queries
      expect(getByAltText("CVC Location")).toBeInTheDocument();
    });
  });
});
