import React from "react";
import {
  ErrorMessage,
  InputContainer,
  InputField,
  InputLabel,
} from "./CustomInputs.style";

interface PhoneInputProps {
  label: string;
  placeholder?: string;
  phone: string;
  setPhone: React.Dispatch<React.SetStateAction<string>>;
  isValidPhone: boolean;
  setIsValidPhone: React.Dispatch<React.SetStateAction<boolean>>;
}

const PhoneInput: React.FC<PhoneInputProps> = ({
  label,
  placeholder,
  phone,
  setPhone,
  isValidPhone,
  setIsValidPhone,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(event.target.value);
    const isValidPhone = /^\d{9}$/.test(phone);
    setIsValidPhone(isValidPhone);
  };

  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <InputField
        type="tel"
        placeholder={placeholder}
        value={phone}
        onChange={handleChange}
        style={{ borderColor: isValidPhone ? "initial" : "red" }}
      />
      {!isValidPhone && (
        <ErrorMessage>
          Ingrese un número de teléfono válido (10 dígitos)
        </ErrorMessage>
      )}
    </InputContainer>
  );
};

export default PhoneInput;
