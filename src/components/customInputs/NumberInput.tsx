import React from "react";
import styled from "styled-components";

interface NumberInputProps {
  value: string;
  onChange: (newValue: string) => void;
}

const NumberInput: React.FC<NumberInputProps> = ({ value, onChange }) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value.replace(/\D/g, "");
    onChange(newValue);
  };

  return <Input type="text" value={value} onChange={handleChange} />;
};

const Input = styled.input`
  padding: 8px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
`;

export default NumberInput;
