import React, { useState } from "react";
import visa from "../../assets/visa.png";
import master from "../../assets/master.png";
import {
  InputContainer,
  InputField,
  InputLabel,
  LogoContainer,
} from "./CustomInputs.style";

interface CreditCardInputProps {
  label: string;
  value: string;
  onChange: (newValue: string) => void;
}

const CreditCardInput: React.FC<CreditCardInputProps> = ({
  value,
  onChange,
  label,
}) => {
  const [cardType, setCardType] = useState<"visa" | "mastercard" | "none">(
    "none"
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newValue = event.target.value;

    newValue = newValue.replace(/\D/g, "");

    newValue = newValue.replace(/(\d{4})/g, "$1-");

    newValue = newValue.replace(/-$/, "");

    onChange(newValue);

    if (/^4/.test(newValue)) {
      setCardType("visa");
    } else if (/^5[1-5]/.test(newValue)) {
      setCardType("mastercard");
    } else {
      setCardType("none");
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const keyValue = event.key;
    if (!/^\d$/.test(keyValue) && keyValue !== "Backspace") {
      event.preventDefault();
    }
  };

  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <InputField
        type="text"
        value={value}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        placeholder="Número de tarjeta de crédito"
        style={{ borderColor: "initial" }}
      />
      {cardType !== "none" && (
        <LogoContainer>
          {cardType === "visa" ? (
            <img src={visa} alt="Visa" width={35} />
          ) : (
            <img src={master} alt="Mastercard" width={35} />
          )}
        </LogoContainer>
      )}
    </InputContainer>
  );
};

export default CreditCardInput;
