import React from "react";
import {
  ErrorMessage,
  InputContainer,
  InputField,
  InputLabel,
} from "./CustomInputs.style";

interface CustomInputProps {
  label: string;
  placeholder?: string;
  mail: string;
  setMail: React.Dispatch<React.SetStateAction<string>>;
  isValidMail: boolean;
  setIsValidMail: React.Dispatch<React.SetStateAction<boolean>>;
}

const CustomInputMail: React.FC<CustomInputProps> = ({
  label,
  placeholder,
  mail,
  setMail,
  isValidMail,
  setIsValidMail,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMail(event.target.value);
    const isValidEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mail);
    setIsValidMail(isValidEmail);
  };

  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <InputField
        type="email"
        placeholder={placeholder}
        value={mail}
        onChange={handleChange}
        style={{ borderColor: isValidMail ? "initial" : "red" }}
      />
      {!isValidMail && <ErrorMessage>Correo electrónico inválido</ErrorMessage>}
    </InputContainer>
  );
};

export default CustomInputMail;
