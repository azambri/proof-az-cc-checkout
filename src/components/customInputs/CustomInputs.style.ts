import styled from "styled-components";
export const InputContainer = styled.div`
  width: 100%;
  margin-bottom: 8px;
  position: relative;
`;
export const InputLabel = styled.label`
  margin-bottom: 8px;
  display: block;
  font-weight: bold;
`;
export const InputField = styled.input`
  width: 90%;
  padding: 8px;
  border: 1px solid #ccc;
  border-radius: 4px;
  font-size: 16px;
  display: flex;
  flex-direction: column;
`;
export const ErrorMessage = styled.span`
  color: red;
  font-size: 12px;
`;
export const LogoContainer = styled.div`
  position: absolute;
  top: 75%;
  right: 20px;
  transform: translateY(-50%);
`;
export const Select = styled.select`
  padding: 8px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
`;
