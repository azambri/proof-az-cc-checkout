import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import TermsAndCondition from "./TermsAndCodition";

describe("TermsAndCondition component", () => {
  test("renders correctly", () => {
    const { getByText, getByLabelText } = render(
      <TermsAndCondition termsAccepted={false} setTermsAccepted={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("Acepto los términos y condiciones")).toBeInTheDocument();
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("Ver términos y condiciones")).toBeInTheDocument();
    expect(
      // eslint-disable-next-line testing-library/prefer-screen-queries
      getByLabelText("Acepto los términos y condiciones")
    ).toBeInTheDocument();
  });

  test("handles checkbox change correctly", () => {
    const setTermsAccepted = jest.fn();

    const { getByLabelText } = render(
      <TermsAndCondition
        termsAccepted={false}
        setTermsAccepted={setTermsAccepted}
      />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const checkbox = getByLabelText("Acepto los términos y condiciones");
    fireEvent.click(checkbox);

    expect(setTermsAccepted).toHaveBeenCalledWith(true);
  });

  test("displays modal on terms link click", async () => {
    const { getByText } = render(
      <TermsAndCondition termsAccepted={false} setTermsAccepted={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const termsLink = getByText("Ver términos y condiciones");
    fireEvent.click(termsLink);
  });
});
