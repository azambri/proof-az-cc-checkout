import React from "react";
import { render, fireEvent } from "@testing-library/react";
import MonthSelector from "./MonthSelector";

describe("MonthSelector component", () => {
  test("renders correctly", () => {
    const { getByText } = render(
      <MonthSelector selectedMonth={1} onSelectMonth={() => {}} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("1 - Enero")).toBeInTheDocument();
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByText("12 - Diciembre")).toBeInTheDocument();
  });

  test("handles month selection correctly", () => {
    const handleSelectMonth = jest.fn();

    const { getByRole } = render(
      <MonthSelector selectedMonth={1} onSelectMonth={handleSelectMonth} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.change(getByRole("combobox"), { target: { value: "2" } });

    expect(handleSelectMonth).toHaveBeenCalledWith(2);
  });
});
