import React from "react";
import { Select } from "./CustomInputs.style";

interface InstallmentInputProps {
  value: number;
  onChange: (newValue: number) => void;
}

const InstallmentInput: React.FC<InstallmentInputProps> = ({
  value,
  onChange,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const newValue = parseInt(event.target.value);
    onChange(newValue);
  };

  const options = Array.from({ length: 48 }, (_, index) => index + 1);

  return (
    <Select value={value} onChange={handleChange}>
      {options.map((option) => (
        <option key={option} value={option}>
          {option}
        </option>
      ))}
    </Select>
  );
};

export default InstallmentInput;
