import React from "react";
import { Select } from "./CustomInputs.style";

interface YearSelectorProps {
  selectedYear: number;
  onSelectYear: (year: number) => void;
}

const YearSelector: React.FC<YearSelectorProps> = ({
  selectedYear,
  onSelectYear,
}) => {
  const currentYear = new Date().getFullYear();
  const years = Array.from({ length: 13 }, (_, index) => currentYear + index);

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedYear = parseInt(event.target.value);
    onSelectYear(selectedYear);
  };

  return (
    <Select value={selectedYear} onChange={handleChange}>
      {years.map((year) => (
        <option key={year} value={year}>
          {year}
        </option>
      ))}
    </Select>
  );
};

export default YearSelector;
