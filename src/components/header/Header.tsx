import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../store/store";
import { HeaderProps } from "./model";
import { HeaderContainer, Logo, Button } from "./Header.styles";
import { abrirModal } from "../../store/slice/ProductSlice";

const Header: React.FC<HeaderProps> = ({ logoSrc, buttonText }) => {
  const dispatch = useDispatch();
  const productoSeleccionado = useSelector(
    (state: RootState) => state.productos.productoSeleccionado
  );
  const handleOpenModal = () => {
    dispatch(abrirModal());
  };
  return (
    <>
      <HeaderContainer>
        <Logo src={logoSrc} data-testid="logo" />
        {productoSeleccionado && (
          <Button onClick={handleOpenModal} data-testid="Button">
            {buttonText}
          </Button>
        )}
      </HeaderContainer>
    </>
  );
};

export default Header;
