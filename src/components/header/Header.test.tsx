import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import Header from "./Header";
import { store } from "../../store/store";

describe("Header component", () => {
  test("renders correctly with selected product", () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <Header logoSrc="logo.png" buttonText="Button" />
      </Provider>
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByTestId("logo")).toBeInTheDocument();
  });
});
