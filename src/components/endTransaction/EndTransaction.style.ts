import styled from "styled-components";
export const EndTransactionContainer = styled.div`
  display: flex;
  .loading {
    display: flex;
    flex-direction: column;
  }
`;
export const TransaccionOkContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

export const AprobadoMessage = styled.h2`
  color: red;
  font-size: 24px;
  margin-bottom: 20px;
`;

export const ResumenCompra = styled.div`
  max-width: 600px;
  text-align: justify;
`;
