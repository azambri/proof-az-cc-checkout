import React from "react";
import SumaryProduct from "../../summaryProduct/SummaryProduct";
import { Button } from "../../header/Header.styles";
import { cerrarModal } from "../../../store/slice/ProductSlice";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  AprobadoMessage,
  TransaccionOkContainer,
} from "../EndTransaction.style";

const TransactionOk: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleCorrection = () => {
    navigate("/");
    dispatch(cerrarModal());
  };
  return (
    <TransaccionOkContainer>
      <AprobadoMessage>Aprobado</AprobadoMessage>
      <p>Resumen de la compra realizada:</p>
      <SumaryProduct />
      <Button onClick={handleCorrection}>Volver a comprar</Button>
    </TransaccionOkContainer>
  );
};

export default TransactionOk;
