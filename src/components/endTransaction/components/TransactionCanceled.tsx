import React from "react";
import { Button } from "../../header/Header.styles";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { abrirModal } from "../../../store/slice/ProductSlice";
import {
  AprobadoMessage,
  ResumenCompra,
  TransaccionOkContainer,
} from "../EndTransaction.style";

const TransactionCanceled: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleCorrection = () => {
    navigate("/");
    dispatch(abrirModal());
  };
  return (
    <TransaccionOkContainer>
      <AprobadoMessage>Tarjeta reachazada</AprobadoMessage>
      <ResumenCompra>
        <Button onClick={handleCorrection}>Corregir datos</Button>
      </ResumenCompra>
    </TransaccionOkContainer>
  );
};

export default TransactionCanceled;
