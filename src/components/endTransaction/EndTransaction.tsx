import { useState, useEffect } from "react";
import Loading from "../loading/Loading";
import { RootState } from "../../store/store";
import { Title } from "../modal/components/stepTwo/StepTwo.style";
import { EndTransactionContainer } from "./EndTransaction.style";
import TransactionOk from "./components/TransactionOk";
import TransactionCanceled from "./components/TransactionCanceled";
import { useSelector } from "react-redux";

const EndTransaction = () => {
  const cardData = useSelector((state: RootState) => state.card.cardData);
  const [loading, setloading] = useState(true);
  const [transaction, setTransaction] = useState(true);
  useEffect(() => {
    handleValidatedCredictCard();
    setTimeout(() => {
      setloading(false);
    }, 3000);
  }, []);

  const handleValidatedCredictCard = () => {
    if (
      (cardData.numberCredit === "4111-1111-1111-1111" ||
        cardData.numberCredit === "5111-1111-1111-1111") &&
      cardData.idNumber === "80123456" &&
      cardData.cvc === "123" &&
      cardData.selectedMonth === 8 &&
      cardData.selectedYear === 2025
    ) {
      setTransaction(true);
    } else {
      setTransaction(false);
    }
  };

  return (
    <EndTransactionContainer>
      {loading ? (
        <div className="loading">
          <Title>Enviando Transacción</Title>
          <Loading />
        </div>
      ) : transaction ? (
        <TransactionOk />
      ) : (
        <TransactionCanceled />
      )}
    </EndTransactionContainer>
  );
};

export default EndTransaction;
