import { RouteObject } from "react-router-dom";
import StartPage from "../pages/startPage/index";
import EndPage from "../pages/endPage/index";

const routerMap: RouteObject[] = [
  { path: "/", element: <StartPage /> },
  { path: "/end-page", element: <EndPage /> },
];
export default routerMap;
