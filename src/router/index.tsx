import { BrowserRouter, useRoutes } from "react-router-dom";
import routes from "./routerMap";

const Routes = () => useRoutes(routes);
const Router = () => {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
};

export default Router;
