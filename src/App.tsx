import AppContainer from "./styles";
import "./App.css";
import logo from "./assets/cash-register.png";
import Header from "./components/header/Header";
import Footer from "./components/Footer/Footer";
import { Provider } from "react-redux";
import Routes from "./router";
import { store } from "../src/store/store";

function App() {
  return (
    <Provider store={store}>
      <AppContainer>
        <Header logoSrc={logo} buttonText="Comprar" />
        <Routes />
        <Footer />
      </AppContainer>
    </Provider>
  );
}

export default App;
