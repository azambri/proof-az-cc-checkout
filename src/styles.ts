import styled from "styled-components";
const AppContainer = styled.div`
  margin-top: 8vh;
  display: flex;
  flex-direction: column;
  -webkit-box-pack: center;
  justify-content: flex-start;
  -webkit-box-align: center;
  align-items: center;
  background: "#ffffff";
`;
export default AppContainer;
