import styled from "styled-components";

export const StarPageContainer = styled.div`
  display: flex;
  margin: 16px;
  padding-bottom: 32px;
`;
