import { StarPageContainer } from "./StartPage.styles";

import ProductComponent from "../../components/product/ProductComponent";
import ModalCheckout from "../../components/modal/ModalCheckout";

const index = () => {
  return (
    <StarPageContainer>
      <ProductComponent />
      <ModalCheckout />
    </StarPageContainer>
  );
};

export default index;
