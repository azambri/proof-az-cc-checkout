import EndTransaction from "../../components/endTransaction/EndTransaction";
import { EndPageContainer } from "./EndPage.styles";

const index = () => {
  return (
    <EndPageContainer>
      <EndTransaction />
    </EndPageContainer>
  );
};

export default index;
