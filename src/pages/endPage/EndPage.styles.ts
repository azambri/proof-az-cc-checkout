import styled from "styled-components";

export const EndPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 32px 16px;
  padding-bottom: 32px;
`;
